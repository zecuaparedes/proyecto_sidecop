<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('historial_documentos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('documento_id');
            $table->string('estado_anterior');
            $table->string('estado_nuevo');
            //$table->string('estado');
            $table->timestamp('fecha')->nullable();
            $table->timestamps();

            $table->foreign('documento_id')->references('id')->on('documentos');

           
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('historial_documentos');
    }
};
