<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('acuses', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_acuse');
            $table->unsignedBigInteger('documento_id')->nullable();
            $table->string('nombre_firmante')->nullable();
            $table->date('fecha_firma')->nullable();
            $table->timestamps();


            //Nueva relación con la tabla documentos
            
            $table->unsignedBigInteger('documento_contestacion_id')->nullable();
            $table->foreign('documento_id')->references('id')->on('documentos');
            $table->foreign('documento_contestacion_id')->references('id')->on('documentos_contestaciones');
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('acuses');
    }
};
