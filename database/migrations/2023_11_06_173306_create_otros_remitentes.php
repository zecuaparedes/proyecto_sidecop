<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('otros_remitentes', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_del_remitente');
            $table->string('apellidos_remitente');
            $table->string('direccion');
            $table->string('telefono_remitente');
            $table->string('grado_de_estudios');
            $table->string('email')->unique();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('otros_remitentes');
    }
};
