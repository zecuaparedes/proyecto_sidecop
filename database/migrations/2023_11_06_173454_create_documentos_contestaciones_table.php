<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    // TODO: tabla de documentos_contestados proporcionar retroalimentacion LIC GAYTAN
    public function up(): void
    {
        Schema::create('documentos_contestaciones', function (Blueprint $table) {
            $table->id();
              // Campos específicos para 'documentos_contestados'
            $table->string('documento_id')->uniqid();
            $table->string('nombre_documento');
            $table->text('respuesta')->nullable();
            $table->date('fecha_respuesta')->nullable();
            $table->string('responsable')->nullable();
            $table->timestamps();
            /* 
            TODO: Definir los atributos de las documentos contestaciones.
            */

            // --Datos referencia documento--.

            // -- A) Datos Generales--

            // -- B) Datos generador-- (usuario)

            // -- C) Datos autorizacion-- (usuario, atributos de autorizacion como UUID, firma electronica, etc.)

            // -- D) Datos destinatario-- (TODO: evaluar si esta informacion debe contenerse en una tabla diversa debido a que pueden existir diversos destinatarios).

            // -- E) Peticion relacionada--

            // -- F) Archivo acuse--

            // -- G) Actividades relacionadas-- (TODO: Posiblemente este campo deba ir en una tabla diversa que relacione una contenstacion con muchas actividades).

            // -- H) Contenido contestacion--

            // -- I) Datos entrega--

            // -- J) Campos automaticos--(libro de gobierno).

            // -- K) Ultimo movimiento--

            // -- L) Datos de auditoria--
        
          
        
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('contestaciones');
    }
};
