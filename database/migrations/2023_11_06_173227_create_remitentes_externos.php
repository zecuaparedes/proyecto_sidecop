<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('remitentes_externos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellidos_del_remitente');
            $table->string('area');
            $table->string('telefono');
            $table->string('cargo');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('remitentes_externos');
    }
};
