<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     *TABLA DE LA BASE DE DATOS DE LOS REMITENTES_EXTERNOS.
     */
    public function up(): void
    {
        Schema::create('remitentes_pjet', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_remitente');
            $table->string('apellidos');
            $table->string('puesto');
            $table->string('telefono_del_remitente');
            $table->string('direccion_del_remitente');
            $table->string('correo_electronico')->unique();
            $table->timestamps();
        });
    }

    /**
     * RESERVA LA MIGRACION.
     */
    public function down(): void
    {
        Schema::dropIfExists('remitentes_pjet');
    }
};
