<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('areas_poder_judicial', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_del_area');
            $table->string('cargo_del_area');
            $table->string('direccion');
            $table->string('nombre_del_titular');
            $table->string('primer_apellido');
            $table->string('segundo_apellido');
            $table->string('telefono');
            $table->string('puesto');
            $table->string('correo_electronico')->unique();
            $table->timestamps();

            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('areas_poder_judicial');
    }
};
