<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    // TODO: proporcionar retroalimentacion de la tabla destinatarios LIC GAYTAN
    public function up(): void
    {
        Schema::create('destinatarios', function (Blueprint $table) {
            $table->id();
            // Datos persona
            $table->string('nombre');
            $table->string('apellidos');
            $table->string('puesto');
            $table->string('telefono');
            $table->string('direccion');
            $table->string('correo_electronico')->unique();
          
            // Datos cargo
            $table->string('puesto_cargo')->nullable();
            $table->string('nivel_cargo')->nullable();
            $table->string('tipo_cargo')->nullable();
            $table->string('segundo_apellido')->nullable();
            // Datos contacto
            $table->string('telefono_personal')->nullable();
            $table->string('telefono_institucional')->nullable();
            $table->string('telefono_institucional_extension')->nullable();
            $table->string('correo_electronico_institucional')->nullable();
            $table->string('correo_electronico_personal')->nullable();
            $table->string('direccion_area')->nullable();
            // Datos de auditoria
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('destinatarios');
    }
};
