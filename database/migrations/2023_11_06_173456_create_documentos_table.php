<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('documentos', function (Blueprint $table) {
            $table->id(); 
            $table->string('nombre_entrante'); // remitente
            $table->string('asunto'); // asunto
            $table->string('ubicacion_del_departamento'); // ubicacion
            $table->date('fechado');
            $table->date('recibido'); // recibido
            $table->string('numero_de_documento');
            $table->string('numero_de_folio'); // Numero 
            $table->string('a_quien_va_dirigido');
            $table->string('estado')->default('entrantes');
            $table->boolean('importante')->default(false);
            //$table->string('clave_documento')->nullable();
            //$table->string('nombre_documento')->nullable(); // Nuevo campo para el nombre del documento
            $table->unsignedBigInteger('documentos_contestaciones_id')->nullable();
            $table->unsignedBigInteger('destinatarios_id')->nullable();
            $table->unsignedBigInteger('areas_poder_judicial_id')->nullable();
            $table->unsignedBigInteger('remitentes_pjet_id')->nullable();
            $table->unsignedBigInteger('remitentes_externos_id')->nullable();
            $table->unsignedBigInteger('otros_remitentes_id')->nullable();
            $table->unsignedBigInteger('estado_id')->default(1);
            //$table->text('contenido_del_documento')->nullable();
            //$table->string('expediente_primario')->nullable(); 
            //$table->string('expediente_secundario')->nullable();
            $table->timestamps();

            $table->foreign('documentos_contestaciones_id')->references('id')->on('documentos_contestaciones');
            $table->foreign('remitentes_pjet_id')->references('id')->on('remitentes_pjet');
            $table->foreign('estado_id')->references('id')->on('cat_estados'); 
            $table->foreign('remitentes_externos_id')->references('id')->on('remitentes_externos');
            $table->foreign('otros_remitentes_id')->references('id')->on('otros_remitentes');
            $table->foreign('areas_poder_judicial_id')->references('id')->on('areas_poder_judicial');
            $table->foreign('destinatarios_id')->references('id')->on('destinatarios');
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('documentos');
    }
};
