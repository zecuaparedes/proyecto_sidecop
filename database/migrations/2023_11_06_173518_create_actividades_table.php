<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    // TODO: tabla de actividades proporcionar retroalimentacion LIC GAYTAN
    public function up(): void
    {
        Schema::create('actividades', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_entrante'); // remitente
            $table->string('asunto'); // asunto
            $table->string('ubicacion_del_departamento'); // ubicacion
            $table->date('fechado');
            $table->date('recibido'); // recibido
            $table->string('numero_de_documento');
            $table->string('numero_de_folio'); // Numero 
            $table->string('a_quien_va_dirigido')->nullable();
         
            
            $table->unsignedBigInteger('documento_id')->nullable();
            $table->unsignedBigInteger('documento_contestaciones_id')->nullable();
            $table->timestamps();

            $table->foreign('documento_id')->references('id')->on('documentos');
             // Clave foránea que hace referencia a la tabla 'documentos_contestaciones'
             $table->unsignedBigInteger('documento_contestacion_id')->nullable();
             $table->foreign('documento_contestacion_id')->references('id')->on('documentos_contestaciones');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('actividades');
    }
};
