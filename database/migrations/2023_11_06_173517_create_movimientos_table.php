<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('movimientos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('documentos_id');
            $table->string('documento_nombre'); 
            $table->date('fecha_del_movimiento');
            $table->unsignedBigInteger('remitentes_pjet_id');
            $table->unsignedBigInteger('remitentes_externos_id');
            $table->unsignedBigInteger('otros_remitentes_id');
            $table->unsignedBigInteger('areas_poder_judicial_id');
            $table->unsignedBigInteger('destinatarios_id');
            $table->unsignedBigInteger('asignado_a');
            $table->string('usuario_responsable_del_movimiento');
            $table->unsignedBigInteger('tipo_movimiento_id');
            $table->string('tipo_movimiento_nombre');
            $table->string('entrada_salida');
            $table->string('asunto_movimiento');
            $table->timestamps();

            $table->foreign('documentos_id')->references('id')->on('documentos');
            $table->foreign('remitentes_pjet_id')->references('id')->on('remitentes_pjet');
            $table->foreign('remitentes_externos_id')->references('id')->on('remitentes_externos');
            $table->foreign('otros_remitentes_id')->references('id')->on('otros_remitentes');
            $table->foreign('areas_poder_judicial_id')->references('id')->on('areas_poder_judicial');
            $table->foreign('destinatarios_id')->references('id')->on
            ('destinatarios');
            //$table->foreign('tipo_movimiento_id')->references('id')->on('cat_movimientos');
          
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('movimientos');
    }
};
