<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class RemitentesExternosFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nombre' => $this->faker->firstName,
            'apellidos_del_remitente' => $this->faker->lastName,
            'area' => $this->faker->word,
            'telefono' => $this->faker->phoneNumber,
            'cargo' => $this->faker->word,
        ];
    }
}
