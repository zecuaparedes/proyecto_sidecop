<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class OtrosRemitentesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nombre_del_remitente' => $this->faker->firstName,
            'apellidos_remitente' => $this->faker->lastName,
            'direccion' => $this->faker->address,
            'telefono_remitente' => $this->faker->phoneNumber,
            'grado_de_estudios' => $this->faker->word,
            'email' => $this->faker->unique()->safeEmail,
        ];
    }
}
