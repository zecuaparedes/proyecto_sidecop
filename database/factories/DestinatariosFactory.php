<?php

namespace Database\Factories;
use App\Models\Destinatarios;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Destinatarios>
 */
class DestinatariosFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nombre' => $this->faker->firstName,
            'apellidos' => $this->faker->lastName,
            'puesto' => $this->faker->jobTitle,
            'telefono' => $this->faker->phoneNumber,
            'direccion' => $this->faker->address,
            'correo_electronico' => $this->faker->unique()->safeEmail,
        ];
    }
}
