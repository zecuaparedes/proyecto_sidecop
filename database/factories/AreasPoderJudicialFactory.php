<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class AreasPoderJudicialFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nombre_del_area' => $this->faker->word,
            'cargo_del_area' => $this->faker->word,
            'direccion' => $this->faker->address,
            'nombre_del_titular' => $this->faker->name,
            'primer_apellido' => $this->faker->lastName,
            'segundo_apellido' => $this->faker->lastName,
            'telefono' => $this->faker->phoneNumber,
            'puesto' => $this->faker->word,
            'correo_electronico' => $this->faker->unique()->safeEmail,
        ];
    }
}
