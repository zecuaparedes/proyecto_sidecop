<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Documentos>
 */
class DocumentosFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nombre_entrante' => $this->faker->name,
            'asunto' => $this->faker->sentence,
            'ubicacion_del_departamento' => $this->faker->word,
            'fechado' => $this->faker->date,
            'recibido' => $this->faker->date,
            'numero_de_documento' => $this->faker->numerify('Doc-###'),
            'numero_de_folio' => $this->faker->numerify('Folio-###'),
            'a_quien_va_dirigido' => $this->faker->name,
            'nombre_documento' => $this->faker->name,
            //'a_quien_se_turna' => $this->faker->name,
        ];
    }
}
