<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class RemitentesPjetFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nombre_remitente' => $this->faker->firstName,
            'apellidos' => $this->faker->lastName,
            'puesto' => $this->faker->word,
            'telefono_del_remitente' => $this->faker->phoneNumber,
            'direccion_del_remitente' => $this->faker->address,
            'correo_electronico' => $this->faker->unique()->safeEmail,
        ];
    }
}
