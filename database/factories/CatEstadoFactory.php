<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\CatEstado>
 */
class CatEstadoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'entrantes' => $this->faker->numberBetween(0, 100),
            'pendientes' => $this->faker->numberBetween(0, 100),
            'contestados' => $this->faker->numberBetween(0, 100),
            'archivados' => $this->faker->numberBetween(0, 100),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
