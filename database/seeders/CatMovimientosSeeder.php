<?php

namespace Database\Seeders;

use App\Models\Cat_Movimientos;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CatMovimientosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
         // Define datos de ejemplo para insertar en la tabla
         Cat_Movimientos::create( 
            [
                'tipo_movimiento' => 'Tipo Movimiento 1',
                'tipo_activo' => 'Activo 1',
                
                ]);
                
                
    }
}
