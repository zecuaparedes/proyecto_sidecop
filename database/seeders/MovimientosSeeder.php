<?php

namespace Database\Seeders;

use App\Models\Movimientos;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MovimientosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i =1; $i <= 10; $i++) { 
            Movimientos::create([
                'documentos_id' => $i, 
                'documento_nombre' => 'Nombre Documento 1'.$i,
                'fecha_del_movimiento' => now(),
                'remitentes_externos_id' => $i, 
                'remitentes_poder_judicial_id' => $i, 
                'destinatarios_id' => $i, 
                'usuario_responsable_del_movimiento' => 'Usuario Responsable 1'.$i,
                'tipo_movimiento_id' => $i, 
                'tipo_movimiento_nombre' => 'Tipo Movimiento 1'.$i,
                'entrada_salida' => 'Entrada',
                'asunto_movimiento' => 'Asunto 1'.$i,
            ]);  
        }    
    }
}
