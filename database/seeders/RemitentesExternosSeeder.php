<?php

namespace Database\Seeders;

use App\Models\Remitentes_Externos;
use Database\Factories\RemitentesExternosFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RemitentesExternosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void

    {
        for ($i = 1; $i <= 10; $i++) {
            Remitentes_Externos::create([
                'nombre' => 'Remitente ' . $i,
                'apellidos_del_remitente' => 'Apellidos ' . $i,
                'area' => 'Área ' . $i,
                'telefono' => 'Teléfono ' . $i,
                'cargo' => 'Cargo ' . $i,
                
            ]);
        }

        // Utiliza la factory para insertar registros en la tabla
        //RemitentesExternosFactory::factory()->count(10)->create();

    }
}
