<?php

namespace Database\Seeders;

use App\Models\Documentos;
use Database\Factories\DocumentosFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DocumentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i <= 10; $i++) {
            // Generar una clave única basada en la fecha y el tiempo
            //$claveDocumentoUnica = now()->format('YmdHis') . '-' . $i;
            Documentos::create([
                'nombre_entrante' => 'Entrante ' . $i,
                'asunto' => 'Asunto ' . $i,
                'ubicacion_del_departamento' => 'Ubicación ' . $i,
                'fechado' => now(),
                'recibido' => now(),
                'numero_de_documento' => 'Doc-' . $i,
                'numero_de_folio' => 'Folio-' . $i,
                'a_quien_va_dirigido' => 'Destinatario ' . $i,
                //'nombre_documento' => 'nombre' . $i,
                //'clave_documento' => $claveDocumentoUnica,
                
            ]);
        }
        //DocumentosFactory::factory()->count(10)->create();

    }
}
