<?php

namespace Database\Seeders;

use App\Models\Areas_Poder_Judicial;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Database\Factories\AreasPoderJudicialFactory;
use Illuminate\Database\Seeder;

class AreasPoderJudicialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 0; $i < 10; $i++) {
            Areas_Poder_Judicial::create([
                'nombre_del_area' => 'Area ' . ($i + 1),
                'cargo_del_area' => 'Cargo ' . ($i + 1),
                'direccion' => 'Dirección ' . ($i + 1),
                'nombre_del_titular' => 'Titular ' . ($i + 1),
                'primer_apellido' => 'Apellido1 ' . ($i + 1),
                'segundo_apellido' => 'Apellido2 ' . ($i + 1),
                'puesto' => 'Puesto ' . ($i + 1),
                'telefono' => 'Teléfono ' . ($i + 1),
                'correo_electronico' => 'correo' . ($i + 1) . '@example.com',
            ]);
        }
        
        //AreasPoderJudicialFactory::factory()->count(10)->create();

    }
}
