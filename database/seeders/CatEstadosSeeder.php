<?php

namespace Database\Seeders;

use App\Models\CatEstado;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CatEstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
         // Define datos de ejemplo para insertar en la tabla

        
    
         // TODO: proporcionar ejemplo y retroalimentacion para la poblacion de la tabla de base de datos de acuerdo al estado de un documento y para que asu vez se vean reflejados los cambios del documento en entrantes pendientes etc.

         // Entrante
         // Pendiente.
         // Contestado.
         // Archivado.

         $estados = [
            ['nombre' => 'entrate', 'descripcion' => 'Estado de documento: Entrate', 'activo' => true],
            ['nombre' => 'pendiente', 'descripcion' => 'Estado de documento: Pendiente', 'activo' => true],
            ['nombre' => 'contestado', 'descripcion' => 'Estado de documento: Contestado', 'activo' => true],
            ['nombre' => 'archivado', 'descripcion' => 'Estado de documento: Archivado', 'activo' => true],
        ];

         // Inserta los registros en la tabla utilizando el modelo
         foreach ($estados as $estado) {
            CatEstado::create($estado);
        }
        
        // CatEstado::factory()->count(10)->create(); 
         
                
    }
}
