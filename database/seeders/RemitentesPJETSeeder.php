<?php

namespace Database\Seeders;

use App\Models\Remitentes_PJET;
use Database\Factories\RemitentesPjetFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RemitentesPJETSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i <= 10; $i++) {
            Remitentes_PJET::create([
                'nombre_remitente' => 'Remitente PJET ' . $i,
                'apellidos' => 'Apellidos ' . $i,
                'puesto' => 'Puesto ' . $i,
                'telefono_del_remitente' => 'Teléfono ' . $i,
                'direccion_del_remitente' => 'Dirección ' . $i,
                'correo_electronico' => 'remitente_pjet' . $i . '@example.com',
                
            ]);
        }

        //RemitentesPjetFactory::factory()->count(10)->create();
    }
}
