<?php

namespace Database\Seeders;
 use Illuminate\Database\Console\Seeds\WithoutModelEvents;

// use App\Http\Controllers\AreasPoderJudicialController;
use App\Models\Remitentes_PJET;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        
        $this->call([
            // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
            CatMovimientosSeeder::class,
            RemitentesExternosSeeder::class,
            CatEstadosSeeder::class,
            RemitentesPJETSeeder::class,
            AreasPoderJudicialSeeder::class,
            DestinatariosSeeder::class,
            OtrosRemitentesSeeder::class,
            DocumentosSeeder::class,
            // MovimientosSeeder::class,
            
           
        ]);
    }
}
