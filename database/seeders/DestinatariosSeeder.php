<?php

namespace Database\Seeders;

use App\Models\Destinatarios;
use Database\Factories\DestinatariosFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DestinatariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i <= 10; $i++) {
            Destinatarios::create([
                'nombre' => 'Destinatario ' . $i,
                'apellidos' => 'Apellidos ' . $i,
                'puesto' => 'Puesto ' . $i,
                'telefono' => 'Teléfono ' . $i,
                'direccion' => 'Dirección ' . $i,
                'correo_electronico' => 'destinatario' . $i . '@example.com',
                
            ]);
        }

        //DestinatariosFactory::factory()->count(10)->create();     
    }
    
}
