<?php

namespace Database\Seeders;

use Database\Factories\OtrosRemitentesFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Otros_Remitentes;
use Illuminate\Database\Seeder;

class OtrosRemitentesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i <= 10; $i++) {
            Otros_Remitentes::create([
                'nombre_del_remitente' => 'Remitente ' . $i,
                'apellidos_remitente' => 'Apellidos ' . $i,
                'direccion' => 'Dirección ' . $i,
                'telefono_remitente' => 'Teléfono ' . $i,
                'grado_de_estudios' => 'Estudios ' . $i,
                'email' => 'remitente' . $i . '@example.com',
                
            ]);
        }

        // Utiliza la factory para insertar registros en la tabla
        //OtrosRemitentesFactory::factory()->count(10)->create();
    }
}
