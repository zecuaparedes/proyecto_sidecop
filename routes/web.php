<?php

// TODO:AQUI SE EN CUENTRAN LAS DIRECCIONES DE TODOS LOS CONTROLADORES DE LA APLICACCION

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\AreasPoderJudicialController;
use App\Http\Controllers\DestinatariosController;
use App\Http\Controllers\DocumentosPeticionesController;
use App\Http\Controllers\MovimientosController;
use App\Http\Controllers\RemitentesController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\EntrantesController;
use App\Http\Controllers\PendientesController;
use App\Http\Controllers\EnviadosController;
use App\Http\Controllers\ArchivadosController;
use App\Http\Controllers\DocumentosImportantesController;
use App\Http\Controllers\AuthController;


// TODO: AQUI ES DONDE SE ENCUENTRAN TODAS LAS RUTAS DE LA APLICACCION

/**
 * Rutas Areas. 
 * Controlador: AreasPoderJudicialController
 **/

Route::get('areas-poder-judicial/', [AreasPoderJudicialController::class, 'Areas'])->name('AreasPoderJudicial.index');
Route::get('agregararea/', [AreasPoderJudicialController::class, 'agregarArea'])->name('AgregarArea.index');
Route::get('ver_area_poder_judicial/', [AreasPoderJudicialController::class, 'verArea'])->name('VerArea.index');
Route::get('/verAreas', [AreasPoderJudicialController::class, 'obtenerAreas'])->name('obtenerAreas.index');

//TODO: Ruta para buscar algun destinatario mediante la barra de busqueda.
Route::get('/buscarAreas/search', [AreasPoderJudicialController::class, 'buscarAreas'])->name('buscarAreas.index');

Route::post('ver_area_poder_judicial/insert', [AreasPoderJudicialController::class, 'insert'])->name('VerArea.insert');
// Eliminar Area.
Route::delete('/eliminarArea/{id}', [AreasPoderJudicialController::class, 'eliminarArea'])
    ->name('eliminar.area');
// Editar Area.
Route::get('/editar-area/{id}', [AreasPoderJudicialController::class, 'obtenerDetallesArea'])->name('editar.area');
// Actualizar Area.
Route::put('/actualizar-area/{id}', [AreasPoderJudicialController::class, 'actualizarArea'])->name('actualizar.area');


/**
 * //TODO: Rutas Destinatarios.
 * Controlador: DestinatariosController
 * */
Route::get('destinatarios/', [DestinatariosController::class, 'index'])->name('Destinatarios.index');
Route::get('agregardestinatario/', [DestinatariosController::class, 'agregarDestinatario'])->name('AgregarDestinatario.index');
Route::get('verdestinatarios/', [DestinatariosController::class, 'verDestinatario'])->name('VerDestinatario.index');
Route::get('/verDestinatarios', [DestinatariosController::class, 'obtenerDestinatarios'])->name('obtenerDestinatarios.index');

//TODO: Ruta para buscar algun destinatario mediante la barra de busqueda.
Route::get('/verDestinatarios/search', [DestinatariosController::class, 'buscarDestinatarios'])->name('buscarDestinatarios.index');

Route::post('verdestinatarios/insert', [DestinatariosController::class, 'insert'])->name('VerDestinatario.insert');
// Eliminar Destinatario.
Route::delete('/eliminarDestinatario/{id}', [DestinatariosController::class, 'eliminarDestinatario'])
    ->name('eliminar.destinatario');
// Editar Destinatario.
Route::get('/editar-destinatario/{id}', [DestinatariosController::class, 'obtenerDetallesDestinatario'])->name('editar.destinatario');
// Actualizar Destinatario.
Route::put('/actualizar-destinatario/{id}', [DestinatariosController::class, 'actualizarDestinatario'])->name('actualizar.destinatario');


/**
 * //TODO: Rutas Remitentes.
 * Controlador: RemitentesController
 */
Route::get('remitentes/', [RemitentesController::class, 'index'])->name('Remitentes.index');
//TODO: REMITENTES PJET
Route::get('agregarremitentes/', [RemitentesController::class, 'agregarRemitentePJET'])->name('AgregarRemitenteNuevo.index');

Route::get('verremitentes/', [RemitentesController::class, 'verRemitente'])->name('VerRemitente.index');
// TODO: rutas para los remitentes externos.
Route::get('agregarremitentesExternos/', [RemitentesController::class, 'agregarRemitenteExterno'])->name('AgregarRemitenteNuevoExterno.index');

Route::get('verremitentesexternos/', [RemitentesController::class, 'verRemitenteExterno'])->name('VerRemitenteExterno.index');

// TODO: RUTAs PARA OTROS REMITENTES
Route::get('agregarotrosremitentes/', [RemitentesController::class, 'agregarOtroRemitente'])->name('AgregarOtroRemitenteNuevo.index');

Route::get('verotrosremitentes/', [RemitentesController::class, 'verOtroRemitente'])->name('VerOtroRemitente.index');

Route::post('verotrosremitentes/insert', [RemitentesController::class, 'insertOtroRemitente'])->name('VerOtroRemitente.insert');

Route::get('/verOtrosRemitentes', [RemitentesController::class, 'obtenerOtrosRemitentes'])->name('obtenerOtrosRemitentes.index');

//TODO: Ruta para buscar otro remitente mediante la barra de busqueda.
Route::get('/verotrosRemitentes/search', [RemitentesController::class, 'buscarOtrosRemitentes'])->name('buscarOtrosRemitentes.index');

Route::delete('/eliminarOtroRemitente/{id}', [RemitentesController::class, 'eliminarOtroRemitente']);

Route::get('/editar-otro-remitente/{id}', [RemitentesController::class, 'obtenerDetallesOtroRemitente'])->name('editar.otroremitente');

Route::put('/actualizar-otro-remitente/{id}', [RemitentesController::class, 'actualizarOtroRemitente'])->name('actualizar.otroremitente');

//TODO: VER REMITENTES insertados
Route::post('verremitentes/insert', [RemitentesController::class, 'insert'])->name('VerRemitente.insert');
// VER REMITENTES EXTERNOS
Route::post('verremitentesexternos/insert', [RemitentesController::class, 'insertRemitenteExterno'])->name('VerRemitenteExterno.insert');
// TODO: OBTENER REMITENTES
Route::get('/verRemitentes', [RemitentesController::class, 'obtenerRemitentesPJET'])->name('obtenerRemitentes.index');
//TODO: Ruta para buscar un remitente PJET mediante la barra de busqueda.
Route::get('/verRemitentesPJET/search', [RemitentesController::class, 'buscarRemitentesPJET'])->name('buscarRemitentes.index');
// OBTENER REMITENTES EXTERNOS
Route::get('/verRemitentesExternos', [RemitentesController::class, 'obtenerRemitentesExternos'])->name('obtenerRemitentesExternos.index');

//TODO: Ruta para buscar un remitente PJET mediante la barra de busqueda.
Route::get('/verRemitentesexternos/search', [RemitentesController::class, 'buscarRemitentesExternos'])->name('buscarRemitentesExternos.index');

// TODO: Eliminar Remitente.
Route::delete('/eliminarRemitente/{id}', [RemitentesController::class, 'eliminarRemitente'])
    ->name('eliminar.remitente');
// ELIMINAR REMITENTE EXTERNO
Route::delete('/eliminarRemitenteexterno/{id}', [RemitentesController::class, 'eliminarRemitenteExterno'])
    ->name('eliminar.remitenteexterno');
// TODO: Editar Remitente.
Route::get('editar-remitente/{id}', [RemitentesController::class, 'obtenerDetallesRemitente'])->name('editar.remitente');
// EDITAR REMITENTE EXTERNO
Route::get('/editar-remitente-externo/{id}', [RemitentesController::class, 'obtenerDetallesRemitenteExterno'])->name('editar.remitenteexterno');
// TODO: Actualizar remitente.
Route::put('/actualizar-remitente/{id}', [RemitentesController::class, 'actualizarRemitente'])->name('actualizar.remitente');
// ACTUALIZAR REMITENTE EXTERNO
Route::put('/actualizar-remitente-externo/{id}', [RemitentesController::class, 'actualizarRemitenteExterno'])->name('actualizar.remitenteexterno');


// TODO: RUTA DE DOCUMENTOS ENTRANTES.
Route::get('entrantes/', [EntrantesController::class, 'agregarNuevo'])->name('Entrantes.index');
// RUTA DE AGREGAR NUEVO DOCUMENTO
Route::get('agregarnuevo/', [EntrantesController::class, 'agregarNuevo'])->name('AgregarNuevo.index');
// Ver Documento
Route::get('vernuevo/', [EntrantesController::class, 'verNuevo'])->name('VerNuevo.index');

Route::get('/verEntrantes', [EntrantesController::class, 'obtenerEntrantes'])->name('obtenerEntrantes.index');

//TODO: Ruta para buscar un documento mediante la barra de busqueda.
Route::get('/verEntrantes/search', [EntrantesController::class, 'buscarEntrantes'])->name('buscarEntrantes.index');
// RUTA DE AGREGAR NUEVO DOCUMENTO
Route::post('agregarnuevo/insert', [EntrantesController::class, 'insert'])->name('VerNuevo.insert');
// Eliminar Documento
Route::delete('/eliminarEntrante/{id}', [EntrantesController::class, 'eliminarEntrante'])
    ->name('eliminar.entrante');
// Editar Documento
Route::get('/editar-entrantes/{id}', [EntrantesController::class, 'obtenerDetallesEntrante'])->name('editar.entrante');
// Ruta para actualizar el documento existente
Route::put('/actualizar-entrante', [EntrantesController::class, 'actualizarEntrante'])->name('actualizar.entrante');

//RUTAS PARA ENVIAR UN ARCHIVO A PENDIENTES.

//Route::get('/verPendientes', [EntrantesController::class, 'obtenerDatosPendientes']);

Route::put('/mover-a-pendientes/{id}', [EntrantesController::class, 'moverAPendientes'])->name('mover-a-pendientes');

// TODO: RUTA DE ARCHIVOS PENDIENTES.

Route::get('pendientes/', [PendientesController::class, 'index'])->name('Pendientes.index');
// Ruta para obtener los datos de entrantes a pendientes
Route::get('/obtener-documentos-pendientes', [PendientesController::class, 'obtenerDocumentosPendientes']);

// Ruta para eliminar documento pendiente
Route::delete('/eliminar-documento-pendiente/{pendienteid}', [PendientesController::class, 'eliminarDocumentoPendiente']);
// Ruta para cargar un documento
Route::post('/Subir-Archivo', [PendientesController::class, 'SubirDocumento']);

Route::put('/mover-a-contestados/{id}', [PendientesController::class, 'moverAContestados'])->name('mover-a-contestados');

// RUTA DE ARCHIVOS ENVIADOS.
Route::get('enviados/', [EnviadosController::class, 'index'])->name('Contestados.index');

Route::get('/obtener-documentos-contestados', [EnviadosController::class, 'obtenerDocumentosContestados']);
// Ruta para eliminar documento contestado
Route::delete('/eliminar-documento-contestado/{documentoId}', [EnviadosController::class, 'eliminarDocumentoContestado']);
// Ruta para cargar un documento
Route::post('/Subir-Acuse', [EnviadosController::class, 'SubirAcuse']);

Route::put('/mover-a-archivados/{id}', [EnviadosController::class, 'moverAarchivados'])->name('mover-a-archivados');




// RUTA DE ARCHIVADOS.
Route::get('archivados/', [ArchivadosController::class, 'index'])->name('Archivados.index');
Route::get('/obtener-documentos-archivados', [ArchivadosController::class, 'obtenerDocumentosArchivados']);
// Ruta para eliminar documento archivado
Route::delete('/eliminar-documento-archivado/{archivadoid}', [ArchivadosController::class, 'eliminarDocumentoArchivado']);

// RUTA DE DOCUMENTOS.
Route::get('documentos/', [DocumentosPeticionesController::class, 'index'])->name('Documentos.index');

Route::get('/documentoshistorial', [DocumentosPeticionesController::class, 'obtenerDocumentos']);
Route::post('/documentos/{documentoId}/cambiar-estado/{nuevoEstado}', [DocumentosPeticionesController::class, 'cambiarEstado']);

// RUTA DE DOCUMENTOS IMPORTANTES.
Route::get('documentosimportantes/', [DocumentosImportantesController::class, 'index'])->name('DocumentosImportantes.index');

Route::put('/documentos/{id}/importante', [EntrantesController::class, 'marcarImportante']);

Route::get('/documentos/importantes', [DocumentosImportantesController::class, 'obtenerDocumentosImportantes']);

// RUTAS ADICIONALES.
Route::get('movimientos/', [MovimientosController::class, 'index'])->name('Movimientos.index');
Route::get('usuarios/', [UsersController::class, 'index'])->name('Users.index');
// RUTA PARA EL INICIO DE SECION DE LA APLICACION.
Route::get('sidecop/', [AuthController::class, 'login'])->name('Login.index');



// RUTAS DE LA APLICACION DE LARAVEL.

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegistrar' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');
});
