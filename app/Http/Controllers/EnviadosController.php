<?php

namespace App\Http\Controllers;

use App\Models\Documentos;
use App\Models\Acuse;
use App\Models\DocumentoContestacion;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Inertia\Inertia;

class EnviadosController extends Controller
{
    public function index()
    {

        return Inertia::render('documentos/contestados', [
            'id_componente' => 3
        ]);
    }



    public function obtenerDocumentosContestados()

    {

        try {
            // TODO: Obtener la lista de documentos con estado 3 (Contestados)
            $contestados = Documentos::where('estado_id', 3)->get();

            // Obtener información adicional de documentos contestados
            $contestadosConArchivo = $contestados->map(function ($documento) {
                $documentoContestacion = DocumentoContestacion::where('documento_id', $documento->id)->first();
                return [
                    'documento' => [
                        'id' => $documento->id,
                        'nombre_entrante' => $documento->nombre_entrante,
                        'asunto' => $documento->asunto,
                        'ubicacion_del_departamento' => $documento->ubicacion_del_departamento,
                        'fechado' => $documento->fechado,
                        'recibido' => $documento->recibido,
                        'numero_de_documento' => $documento->numero_de_documento,
                        'numero_de_folio' => $documento->numero_de_folio,
                        'a_quien_va_dirigido' => $documento->a_quien_va_dirigido,
                    ],
                    'nombre_archivo' => $documentoContestacion ? $documentoContestacion->nombre_documento : null,
                ];
            });

            return response()->json([
                'contestados' => $contestadosConArchivo,
            ]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Error al obtener documentos contestados', 'error' => $e->getMessage()], 500);
        }
    }

    public function subirAcuse(Request $request)
    {
        try {
            // Validar y guardar el archivo
            $request->validate([
                'file' => 'required|mimes:pdf,doc,docx,xlsx,pptx|max:2048',
            ]);

            $nombre_acuse = uniqid();

            // Guardar el archivo en el almacenamiento de Laravel
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension();
            $nombre_acuse = $file->storeAs('uploads', $nombre_acuse . '.' . $extension);

            // Crear y guardar el documento en la base de datos
            $documento = new Acuse();
            $documento->nombre_acuse = $nombre_acuse;
            //$documento->documento_id = $nombre_acuse; // Otra opción: $documento->documento_id = $path;
            $documento->save();

            return response()->json(['message' => 'Archivo cargado correctamente']);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function moverAarchivados(Request $request, $idDocumento)
    {
        try {
            // Buscar el documento por su ID
            $documento = Documentos::findOrFail($idDocumento);

            // Validar y mover el archivo desde pendientes a contestados
            $request->validate([
                //'nombreArchivo' => 'required|string',
                'nombreAcuse' => 'required|string',
            ]);

            // TODO: retornar valores de la request.

            //$nombreArchivo = $request->input('nombreArchivo');
            $nombreAcuse = $request->input('nombreAcuse');

            // Crear o recuperar una instancia del modelo DocumentoContestacion
            //$documentoContestacion = DocumentoContestacion::firstOrNew(['documento_id' => $idDocumento]);

            // Actualizar el campo nombre_documento en la tabla DocumentosContestaciones
            //$documentoContestacion->nombre_documento = $nombreArchivo;

            // Guardar en la base de datos
            //$documentoContestacion->save();

            // Crear o recuperar una instancia del modelo Acuse
            $acuse = Acuse::firstOrNew(['documento_id' => $idDocumento]);

            // Actualizar los campos necesarios en el modelo Acuse
            $acuse->nombre_acuse = $nombreAcuse;

            // Guardar en la base de datos
            $acuse->save();

            // Mover el documento a "contestados"
            $documento->estado_id = 4;
            $documento->save();

            return response()->json(['message' => 'Documento movido a Contestados correctamente']);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Error al mover el documento a Contestados', 'error' => $e->getMessage()], 500);
        }
    }

    /*public function moverAarchivados(Request $request, $idDocumento)
{
    try {
        // Validar los datos de entrada
        $request->validate([
            'nombreArchivo' => 'required|string', 
            'nombreAcuse' => 'required|string', 
        ]);

        // Iniciar una transacción
        DB::beginTransaction();

        // Buscar el documento por su ID
        $documento = Documentos::findOrFail($idDocumento);

        // Guardar el nombre del archivo en DocumentosContestaciones
        $nombreArchivo = $request->input('nombreArchivo');
        $documentoContestacion = DocumentoContestacion::updateOrCreate(
            ['documento_id' => $idDocumento],
            ['nombre_documento' => $nombreArchivo]
        );
        // Actualizar el campo nombre_documento en la tabla DocumentosContestaciones
        $documentoContestacion->nombre_documento = $nombreArchivo;

        // Guardar el nombre del acuse en Acuse
        $nombreAcuse = $request->input('nombreAcuse');
        $acuse = Acuse::updateOrCreate(
            ['documento_id' => $idDocumento],
            ['nombre_acuse' => $nombreAcuse]
        );
        // Actualizar los campos necesarios en el modelo Acuse
        $acuse->nombre_acuse = $nombreAcuse;

        // Actualizar el estado del documento a "contestados"
        $documento->estado_id = 4;
        $documento->save();

        // Confirmar la transacción
        DB::commit();

        return response()->json(['message' => 'Documento movido a Contestados correctamente']);
    } catch (\Exception $e) {
        // Revertir la transacción en caso de error
        DB::rollback();
        return response()->json(['message' => 'Error al mover el documento a Contestados', 'error' => $e->getMessage()], 500);
    }
}*/


    /*public function eliminarDocumentoContestado($contestadoid)

    {

        try {
            // Encuentra el documento a eliminar en DocumentoContestacion
            $documentoContestacion = DocumentoContestacion::where('nombre_documento', $contestadoid)->first();

            if (!$documentoContestacion) {
                return response()->json(['message' => 'Documento no encontrado en DocumentoContestacion'], 404);
            }

            // Obtiene el nombre del documento relacionado
            $documentoNombre = $documentoContestacion->documento_nombre;

            if (!$documentoNombre) {
                return response()->json(['message' => 'Documento relacionado no encontrado en Documentos'], 404);
            }

            // Elimina el registro en DocumentoContestacion
            $documentoContestacion->delete();

            // Elimina el documento en Documentos
            $documento = Documentos::where('nombre_documento', $documentoNombre)->first();

            if ($documento) {
                $documento->delete();
            }

            return response()->json(['message' => 'Documento y datos adicionales eliminados con éxito']);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Error al eliminar el documento', 'error' => $e->getMessage()], 500);
        }
    }*/

    public function eliminarDocumentoContestado($id)
    {
        try {
            // Buscar el documento por su ID
            $documento = Documentos::findOrFail($id);
            
            // Buscar el documento contestado por su ID
            $documentoContestado = DocumentoContestacion::findOrFail($id);
    
            // Eliminar el documento
            if ($documento) {
                $documento->delete();
            }
    
            // Eliminar el documento contestado
            if ($documentoContestado) {
                $documentoContestado->delete();
            }
    
            // Devolver una respuesta de éxito
            return response()->json(['message' => 'Documento y documento contestado eliminados correctamente']);
        } catch (\Exception $e) {
            // Manejar errores
            return response()->json(['error' => 'Error al eliminar el documento y documento contestado', 'message' => $e->getMessage()], 500);
        }
    }
    
    


  

}
