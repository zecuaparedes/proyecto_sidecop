<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Destinatarios;

class DestinatariosController extends Controller
{
    public function agregarDestinatario()
    {
        return Inertia::render('formularios/agregardestinatario', [
            'id_componente' => 8
        ]);
    }

    public function insert(Request $request)
    {
        $nuevoDestinatario =  Destinatarios::create(
            [
                'nombre' => $request->datos_escrito_entrantes['nombre'],
                'apellidos' => $request->datos_escrito_entrantes['apellidos'],
                'puesto' => $request->datos_escrito_entrantes['puesto'],
                'telefono' => $request->datos_escrito_entrantes['telefono'],
                'direccion' => $request->datos_escrito_entrantes['direccion'],
                'correo_electronico' => $request->datos_escrito_entrantes['correo_electronico'],

            ]
        );


        return response()->json([
            'data' => $request->datos_escrito_entrantes
        ]);
    }

    public function verDestinatario()
    {
        return Inertia::render('tablas/tabladestinatarios', [
            'id_componente' => 8
        ]);
    }
    public function ver()
    {
        return 0;
    }
    public function obtenerDestinatarios()
    {
        $informacion = Destinatarios::all();
        return response()->json(['destinatarios' => $informacion,]);
    }

    public function eliminarDestinatario($id)

    {
        try {
            // Encuentra el destinatario por su ID
            $remitente = Destinatarios::findOrFail($id);

            // Elimina el destinatario
            $remitente->delete();

            return response()->json(['message' => 'Destinatario eliminado con éxito'], 200);
        } catch (\Exception $e) {
            // error si el destinatario no se elimino
            return response()->json(['error' => 'Error al eliminar el destinatario'], 500);
        }
    }

    public function obtenerDetallesDestinatario($id = 0)
    {
        $destinatario = Destinatarios::findOrFail($id);
        return Inertia::render('formularios/agregardestinatario', [
            'id_componente' => 8,
            'id_destinatario' => $id,
            'destinatario' => $destinatario,
        ]);
    }


     //TODO: funcion para buscar Otro remitente mediante la barra de busqueda.

     public function buscarDestinatarios(Request $request)
     {
         $searchQuery = $request->query('search');
 
         // Realiza la búsqueda en la base de datos usando los campos que deseas buscar
         $resultado = Destinatarios::where('nombre', 'LIKE', "%$searchQuery%")
         ->orWhere('apellidos', 'LIKE', "%$searchQuery%")
         ->orWhere('puesto', 'LIKE', "%$searchQuery%")
         ->orWhere('telefono', 'LIKE', "%$searchQuery%")
         ->orWhere('direccion', 'LIKE', "%$searchQuery%")
         ->orWhere('correo_electronico', 'LIKE', "%$searchQuery%")
         ->get();
 
         return response()->json([
         'destinatarios' => $resultado,
        ]);
     }

    public function actualizarDestinatario(Request $request, $id)
    {
        try {
            // Validación de datos (puedes personalizar según tus necesidades)
            $request->validate([
                'datos_escrito_entrantes.nombre' => 'required|string|max:255',
                'datos_escrito_entrantes.apellidos' => 'required|string|max:255',
                'datos_escrito_entrantes.puesto' => 'required|string|max:255',
                'datos_escrito_entrantes.telefono' => 'required|string|max:255',
                'datos_escrito_entrantes.direccion' => 'required|string|max:255',
                'datos_escrito_entrantes.correo_electronico' => 'required|string|max:255',
                
            ]);
    
            // Encuentra el remitente existente por su ID
            $destinatario = Destinatarios::findOrFail($id);
    
            // Actualiza los campos con los datos del formulario
            $destinatario->update($request->datos_escrito_entrantes);
    
            // Puedes realizar otras operaciones si es necesario
    
            return response()->json(['message' => 'Cambios guardados correctamente en el registro existente']);
        } catch (\Exception $e) {
            // Manejo de errores
            return response()->json(['error' => 'Error al guardar cambios del destinatario existente', 'message' => $e->getMessage()], 500);
        }
    }



}
