<?php

namespace App\Http\Controllers;

use App\Models\DocumentoHistorial;
use Illuminate\Http\Request;
use App\Models\Documentos;
use Inertia\Inertia;
/* 
Clase encargada del manejo de los documentos de peticiones a la direccion.
*/
class DocumentosPeticionesController extends Controller
{
    public function index()
    {
        
        return Inertia::render('documentos/historialdocumentos', [
            'id_componente' => 5
        ]);
    }

    public function cambiarEstado( $documentoId, $nuevoEstado)
    {
        // Encuentra el documento por su ID
        $documento = Documentos::findOrFail($documentoId);

        // Guarda el estado anterior del documento
        $estadoAnterior = $documento->estado;

        // Actualiza el estado del documento
        $documento->estado = $nuevoEstado;
        $documento->save();

        // Registra el cambio de estado en el historial de documentos
        DocumentoHistorial::create([
            'documento_id' => $documentoId,
            'estado_anterior' => $estadoAnterior,
            'estado_nuevo' => $nuevoEstado,
            'fecha' => now(),
        ]);

        return response()->json(['message' => 'Estado del documento actualizado con éxito']);
    }

    public function obtenerDocumentos()
    {
        // Obtén todos los documentos con su historial de cambios de estado
        $documentos = Documentos::with('historialDocumentos')->get();
        return response()->json($documentos);
    }

}
