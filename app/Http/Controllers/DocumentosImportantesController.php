<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Documentos;

class DocumentosImportantesController extends Controller
{
    public function index()
    {
        
        return Inertia::render('documentos/documentosimportantes',[
            'id_componente' => 6
        ]);
    }

    public function obtenerDocumentosImportantes()
    {
        $documentosImportantes = Documentos::where('importante', true)->get();
        return response()->json($documentosImportantes);
    }
}
