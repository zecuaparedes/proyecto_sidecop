<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Areas_Poder_Judicial;
use Illuminate\Support\Facades\DB;

class AreasPoderJudicialController extends Controller
{

    public function Areas()
    {
        
        return Inertia::render('ListaAreas');
    }

    public function agregarArea()
    {

        return Inertia::render('formularios/agregar_area_poder_judicial',[
            'id_componente' => 9
        ]);
    }
    public function insert(Request $request)
    {
        $nuevaArea =  Areas_Poder_Judicial::create(
            [
               'nombre_del_area' => $request->datos_escrito_entrantes['nombre_del_area'],
               'cargo_del_area' => $request->datos_escrito_entrantes['cargo_del_area'],
               'direccion' => $request->datos_escrito_entrantes['direccion'],
               'nombre_del_titular' => $request->datos_escrito_entrantes['nombre_del_titular'],
               'primer_apellido' => $request->datos_escrito_entrantes['primer_apellido'],
               'segundo_apellido' => $request->datos_escrito_entrantes['segundo_apellido'],
               'puesto' => $request->datos_escrito_entrantes['puesto'],
               'telefono' => $request->datos_escrito_entrantes['telefono'],
               'correo_electronico' => $request->datos_escrito_entrantes['correo_electronico'],

            ]
        );
        return response()->json([
            'data' => $request->datos_escrito_entrantes
        ]);

    }

    public function verArea()
    {

        return Inertia::render('tablas/tabla_areas_poder_judicial',[
            'id_componente' => 9
        ]);
    }
    public function ver()
    {
        return 0;
    }

    public function obtenerAreas()
    {
        $verareas = Areas_Poder_Judicial:: all();
        return response()->json([
            'areas' => $verareas,]);
    }

      //TODO: funcion para buscar Otro remitente mediante la barra de busqueda.

      public function buscarAreas(Request $request)
      {
          $searchQuery = $request->query('search');
  
          // Realiza la búsqueda en la base de datos usando los campos que deseas buscar
          $resultado = Areas_Poder_Judicial::where('nombre_del_area', 'LIKE', "%$searchQuery%")
          ->orWhere('cargo_del_area', 'LIKE', "%$searchQuery%")
          ->orWhere('direccion', 'LIKE', "%$searchQuery%")
          ->orWhere('nombre_del_titular', 'LIKE', "%$searchQuery%")
          ->orWhere('primer_apellido', 'LIKE', "%$searchQuery%")
          ->orWhere('segundo_apellido', 'LIKE', "%$searchQuery%")
          ->orWhere('telefono', 'LIKE', "%$searchQuery%")
          ->orWhere('correo_electronico', 'LIKE', "%$searchQuery%")
          ->get();
  
          return response()->json([
          'areas' => $resultado,
         ]);
      }
     

    public function eliminarArea($id)

    {
        try {

            $id = (int) $id;

            DB::beginTransaction();

            // Encuentra el area por su ID
            $area = Areas_Poder_Judicial::findOrFail($id);

            // Elimina el area
            $area->delete();
            // Confirma la transacción si todo fue exitoso
            DB::commit();

            return response()->json(['message' => 'Area Eliminada con Exito'], 200);
        } catch (\Exception $e) {
            // Deshace la transacción en caso de error
            DB::rollBack();
            // error si el area no se elimino
            return response()->json(['error' => 'Error al eliminar el area',
        ], 500);
        }
    }

    public function obtenerDetallesArea($id = 0)
    {
        $area = Areas_Poder_Judicial::findOrFail($id);
        return Inertia::render('formularios/agregar_area_poder_judicial', [
            'id_componente' => 9,
            'id_area' => $id,
            'area' => $area,
        ]);
    }

    public function actualizarArea(Request $request, $id)
    {
        try {
            // Validación de datos
            $request->validate([
                'datos_escrito_entrantes.nombre_del_area' => 'required|string|max:255',
                'datos_escrito_entrantes.cargo_del_area' => 'required|string|max:255',
                'datos_escrito_entrantes.direccion' => 'required|string|max:255',
                'datos_escrito_entrantes.nombre_del_titular' => 'required|string|max:255',
                'datos_escrito_entrantes.primer_apellido' => 'required|string|max:255',
                'datos_escrito_entrantes.segundo_apellido' => 'required|string|max:255',
                'datos_escrito_entrantes.puesto' => 'required|string|max:255',
                'datos_escrito_entrantes.telefono' => 'required|string|max:255',
                'datos_escrito_entrantes.correo_electronico' => 'required|string|max:255',
                
            ]);
    
            // Encuentra el remitente existente por su ID
            $area = Areas_Poder_Judicial::findOrFail($id);
    
            // Actualiza los campos con los datos del formulario
            $area->update($request->datos_escrito_entrantes);
    
            return response()->json(['message' => 'Cambios guardados correctamente en el registro existente']);
        } catch (\Exception $e) {
            // Manejo de errores
            return response()->json(['error' => 'Error al guardar cambios del area existente', 'message' => $e->getMessage()], 500);
        }
    }

}
