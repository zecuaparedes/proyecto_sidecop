<?php

namespace App\Http\Controllers;

use App\Models\Actividades;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Documentos;

class EntrantesController extends Controller
{

    public function agregarNuevo()
    {

        return Inertia::render('formularios/agregarnuevo', [
            'id_componente' => 1
        ]);
    }

    public function insert(Request $request)
    {
        try {

            $nuevoDocumento = Documentos::create([
                'nombre_entrante' => $request->datos_escrito_entrantes['nombre_entrante'],
                'asunto' => $request->datos_escrito_entrantes['asunto'],
                'ubicacion_del_departamento' => $request->datos_escrito_entrantes['ubicacion_del_departamento'],
                'fechado' => $request->datos_escrito_entrantes['fechado'],
                'recibido' => $request->datos_escrito_entrantes['recibido'],
                'numero_de_documento' => $request->datos_escrito_entrantes['numero_de_documento'],
                'numero_de_folio' => $request->datos_escrito_entrantes['numero_de_folio'],
                'a_quien_va_dirigido' => $request->datos_escrito_entrantes['a_quien_va_dirigido'],

            ]);


            return response()->json([
                'data' => $request->datos_escrito_entrantes

            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'message' => 'Hubo un problema al crear el documento. Por favor, inténtalo de nuevo.'
            ], 500);
        }
    }

    public function verNuevo()
    {

        return Inertia::render('documentos/entrantes', [
            'id_componente' => 1
        ]);
    }
    public function ver()
    {
        return 0;
    }

    public function obtenerEntrantes()
    {
        // Filtrar todos los que tengan el estado id = 1
        $resultado = Documentos::where('estado_id', 1)->get();
        return response()->json([
            'entrantes' => $resultado,
        ]);
    }

    public function marcarImportante($idDocumento)
    {
        $documento = Documentos::findOrFail($idDocumento);
        $documento->importante = true;
        $documento->save();

        return response()->json(['message' => 'Documento marcado como importante']);
    }

    //TODO: funcion para buscar algun dato de un documento mediante la barra de busqueda.

    public function buscarEntrantes(Request $request)
    {
        $Buscar = $request->query('search');

        // Realiza la búsqueda en la base de datos usando los campos que deseas buscar
        $resultado = Documentos::where('nombre_entrante', 'LIKE', "%$Buscar%")
            ->orWhere('asunto', 'LIKE', "%$Buscar%")
            ->orWhere('ubicacion_del_departamento', 'LIKE', "%$Buscar%")
            ->orWhere('fechado', 'LIKE', "%$Buscar%")
            ->orWhere('recibido', 'LIKE', "%$Buscar%")
            ->orWhere('numero_de_documento', 'LIKE', "%$Buscar%")
            ->orWhere('numero_de_folio', 'LIKE', "%$Buscar%")
            ->orWhere('a_quien_va_dirigido', 'LIKE', "%$Buscar%")
            ->get();

        return response()->json([
            'entrantes' => $resultado,
        ]);
    }

    public function eliminarEntrante($id)

    {
        try {
            // Encuentra el documento entrante por su ID
            $entrante = Documentos::findOrFail($id);

            // Elimina el documento entrante
            $entrante->delete();

            return response()->json(['message' => 'Documento Entrante eliminado con éxito'], 200);
        } catch (\Exception $e) {
            // error si el documento entrante no se elimino
            return response()->json(['error' => 'Error al eliminar el documento entrante'], 500);
        }
    }

    public function obtenerDetallesEntrante($id = 0)
    {
        $entrante = Documentos::findOrFail($id);
        return Inertia::render('formularios/agregarnuevo', [
            'id_componente' => 1,
            'id_entrante' => $id,
            'entrante' => $entrante,
        ]);
    }

    public function actualizarEntrante(Request $request)
    {
        try {
            // Validación de datos
            $request->validate([
                'datos_escrito_entrantes.nombre_entrante' => 'required|string|max:255',
                'datos_escrito_entrantes.asunto' => 'required|string|max:255',
                'datos_escrito_entrantes.ubicacion_del_departamento' => 'required|string|max:255',
                'datos_escrito_entrantes.fechado' => 'required|string|max:255',
                'datos_escrito_entrantes.recibido' => 'required|string|max:255',
                'datos_escrito_entrantes.numero_de_documento' => 'required|string|max:255',
                'datos_escrito_entrantes.numero_de_folio' => 'required|string|max:255',
                'datos_escrito_entrantes.a_quien_va_dirigido' => 'required|string|max:255',

            ]);

            // Encuentra el documento existente por su ID
            $entrante = Documentos::findOrFail($request->id_documento);

            // TODO: Actualiza los campos con los datos del formulario
            $entrante->update($request->datos_escrito_entrantes);

            return response()->json([
                'message' => 'Cambios guardados correctamente en el registro existente',
                'id_documento' => $request->id_documento,
                'Solicitud' => $request->datos_escrito_entrantes
            ]);
        } catch (\Exception $e) {
            // Manejo de errores
            return response()->json(['error' => 'Error al guardar cambios del Documento existente', 'message' => $e->getMessage()], 500);
        }
    }

    public function moverApendientes(Request $request, $idDocumento)

    {
        try {

            //$idDocumento = $request->query('id');
            //$a_quien_se_turna = $request->input('name');

            // Buscar el documento por su ID
            $documento = Documentos::findOrFail($idDocumento);

            // Actualizar el estado 
            $documento->estado_id = 2;
            // TODO: en que campo guardar el nombre de la persona a la que se turna e invocarlo para guardarlo.
            //$documento->a_quien_se_turna = $a_quien_se_turna;
            $documento->save();

            return response()->json(['message' => 'Documento movido a Pendientes correctamente']);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Error al mover el documento a Pendientes', 'error' => $e->getMessage()], 500);
        }
    }

    


}
