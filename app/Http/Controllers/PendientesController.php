<?php

namespace App\Http\Controllers;

use App\Models\DocumentoContestacion;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Documentos;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PendientesController extends Controller
{
    public function index ()

    {
        return inertia::render('documentos/pendientes',[
            'id_componente' => 2
        ]);
    }

    public function obtenerDocumentosPendientes()

    {
    // Obtener la lista de documentos con estado 2 (Pendientes)
    $pendientes = Documentos::where('estado_id', 2)->get();

    return response()->json([
        'pendientes' => $pendientes,
    ]);

    }

    public function eliminarDocumentoPendiente($id)
    {
        try {
            // Buscar el documento pendiente por ID
            $pendiente = Documentos::findOrFail($id);

            // Eliminar el documento
            $pendiente->delete();

            // Devolver una respuesta de éxito
            return response()->json(['message' => 'Documento eliminado correctamente']);
        } catch (\Exception $e) {
            // Manejar errores
            return response()->json(['error' => 'Error al eliminar el documento', 'message' => $e->getMessage()], 500);
        }
    }

    public function subirDocumento(Request $request)
    {
        try {
            // Validar y guardar el archivo
            $request->validate([
                'file' => 'required|mimes:pdf,doc,docx
                xlsx,pptx|max:2048',
            ]);
    
            $nombre_documento = uniqid();

            // Guardar el archivo en el almacenamiento de Laravel
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension();
            $nombre_documento = $file->storeAs('uploads', $nombre_documento . '.' . $extension);
    
            // Crear y guardar el documento en la base de datos
            $documento = new DocumentoContestacion();
            $documento->nombre_documento = $nombre_documento;
            $documento->documento_id = $nombre_documento; // Otra opción: $documento->documento_id = $path;
            $documento->save();
    
            return response()->json(['message' => 'Archivo cargado correctamente']);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /*public function moverAcontestados($idDocumento)

    {
        try {
        // Buscar el documento por su ID
        $documento = Documentos::findOrFail($idDocumento);

        // Actualizar el estado a "contestados" estado_id 3
        $documento->estado_id = 3;
        
        // Guardar los cambios
        $documento->save();

            return response()->json(['message' => 'Documento movido a Contestados correctamente']);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Error al mover el documento a Contestados', 'error' => $e->getMessage()], 500);
        }
    }*/

    public function moverAContestados(Request $request, $idDocumento)

    {
        try {
        // Buscar el documento por su ID
        $documento = Documentos::findOrFail($idDocumento);

        // Validar y mover el archivo desde pendientes a contestados
        $request->validate([
            'nombreArchivo' => 'required|string', 
        ]);

        $nombreArchivo = $request->input('nombreArchivo');

        // Crear o recuperar una instancia del modelo DocumentoContestacion
        $documentoContestacion = DocumentoContestacion::firstOrNew(['documento_id' => $idDocumento]);

        // Actualizar el campo nombre_documento en la tabla DocumentosContestaciones
        $documentoContestacion->nombre_documento = $nombreArchivo;

        // Otros campos que puedas necesitar actualizar en DocumentosContestaciones
        //$documentoContestacion->campo = valor;

        // Guardar en la base de datos
        $documentoContestacion->save();

        // Mover el documento a "contestados"
        $documento->estado_id = 3; // Ajusta el estado_id según tu configuración
        $documento->save();

        return response()->json(['message' => 'Documento movido a Contestados correctamente']);
    }   catch (\Exception $e) {
        return response()->json(['message' => 'Error al mover el documento a Contestados', 'error' => $e->getMessage()], 500);
    }
    
    }
    

}
