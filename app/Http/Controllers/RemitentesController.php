<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Remitentes_PJET;
use App\Models\Remitentes_Externos;
use App\Models\Otros_Remitentes;
class RemitentesController extends Controller

{  // TODO: metodos para agregar algun tipo de remitentes
    public function agregarRemitentePJET()
    {

        return Inertia::render('formularios/agregarremitentes', [
            'id_componente' => 7
        ]);
    }

    public function agregarRemitenteExterno()
    {
        return Inertia::render('formularios/agregarremitentes', [
            'id_componente' => 10
        ]);
    }

    public function agregarOtroRemitente()
    {
        return Inertia::render('formularios/agregarremitentes', [
            'id_componente' => 11
        ]);
    }

    // TODO: metodos para insertar los datos de algun remitente


    public function insert(Request $request)
    {
        $nuevoRemitente =  Remitentes_PJET::create(
            [
                'nombre_remitente' => $request->datos_escrito_entrantes['nombre_remitente'],
                'apellidos' => $request->datos_escrito_entrantes['apellidos'],
                'puesto' => $request->datos_escrito_entrantes['puesto'],
                'telefono_del_remitente' => $request->datos_escrito_entrantes['telefono_del_remitente'],
                'direccion_del_remitente' => $request->datos_escrito_entrantes['direccion_del_remitente'],
                'correo_electronico' => $request->datos_escrito_entrantes['correo_electronico'],

            ]

        );
        return response()->json([
            'data' => $request->datos_escrito_entrantes
        ]);
    }

    public function insertRemitenteExterno(Request $request)
    {
        $nuevoRemitenteExterno =  Remitentes_Externos::create(
            [
                'nombre' => $request->datos_escrito_entrantes['nombre'],
                'apellidos_del_remitente' => $request->datos_escrito_entrantes['apellidos_del_remitente'],
                'area' => $request->datos_escrito_entrantes['area'],
                'telefono' => $request->datos_escrito_entrantes['telefono'],
                'cargo' => $request->datos_escrito_entrantes['cargo'],


            ]

        );
        return response()->json([
            'data' => $request->datos_escrito_entrantes
        ]);
    }

    public function insertOtroRemitente(Request $request)
    {
        $otroRemitente =  Otros_Remitentes::create(
            [
                'nombre_del_remitente' => $request->datos_escrito_entrantes['nombre_del_remitente'],
                'apellidos_remitente' => $request->datos_escrito_entrantes['apellidos_remitente'],
                'direccion' => $request->datos_escrito_entrantes['direccion'],
                'telefono_remitente' => $request->datos_escrito_entrantes['telefono_remitente'],
                'grado_de_estudios' => $request->datos_escrito_entrantes['grado_de_estudios'],
                'email' => $request->datos_escrito_entrantes['email'],


            ]

        );
        return response()->json([
            'data' => $request->datos_escrito_entrantes
        ]);
    }

    // TODO: metodos para visualizar algun tipo de remitente

    public function verRemitente()
    {

        return Inertia::render('tablas/tablaremitentesagregados', [
            'id_componente' => 7
        ]);
    }

    public function verRemitenteExterno()
    {

        return Inertia::render('tablas/tablaremitentesexternos', [
            'id_componente' => 10
        ]);
    }

    public function verOtroRemitente()
    {

        return Inertia::render('tablas/tablaotrosremitentes', [
            'id_componente' => 11
        ]);
    }

    public function ver()

    {
        return 0;
    }

    

    //TODO: funcion para buscar algun remitente mediante la barra de busqueda.

    public function buscarRemitentesPJET(Request $request)
    {
        $searchQuery = $request->query('search');

        // Realiza la búsqueda en la base de datos usando los campos que deseas buscar
        $resultado = Remitentes_PJET::where('nombre_remitente', 'LIKE', "%$searchQuery%")
        ->orWhere('apellidos', 'LIKE', "%$searchQuery%")
        ->orWhere('puesto', 'LIKE', "%$searchQuery%")
        ->orWhere('telefono_del_remitente', 'LIKE', "%$searchQuery%")
        ->orWhere('direccion_del_remitente', 'LIKE', "%$searchQuery%")
        ->orWhere('correo_electronico', 'LIKE', "%$searchQuery%")
        ->get();

        return response()->json([
        'remitentes' => $resultado,
       ]);
    }
    //TODO: funcion para buscar algun remitente Externo mediante la barra de busqueda.

    public function buscarRemitentesExternos(Request $request)
    {
        $searchQuery = $request->query('search');

        // Realiza la búsqueda en la base de datos usando los campos que deseas buscar
        $resultado = Remitentes_PJET::where('nombre', 'LIKE', "%$searchQuery%")
        ->orWhere('apellidos_del_remitente', 'LIKE', "%$searchQuery%")
        ->orWhere('area', 'LIKE', "%$searchQuery%")
        ->orWhere('telefono_del_remitente', 'LIKE', "%$searchQuery%")
        ->orWhere('cargo', 'LIKE', "%$searchQuery%")
        ->get();

        return response()->json([
        'remitentes' => $resultado,
       ]);
    }

     //TODO: funcion para buscar Otro remitente mediante la barra de busqueda.

     public function buscarOtrosRemitentes(Request $request)
     {
         $searchQuery = $request->query('search');
 
         // Realiza la búsqueda en la base de datos usando los campos que deseas buscar
         $resultado = Remitentes_PJET::where('nombre_del_remitente', 'LIKE', "%$searchQuery%")
         ->orWhere('apellidos_remitente', 'LIKE', "%$searchQuery%")
         ->orWhere('direccion', 'LIKE', "%$searchQuery%")
         ->orWhere('telefono_remitente', 'LIKE', "%$searchQuery%")
         ->orWhere('grado_de_estudios', 'LIKE', "%$searchQuery%")
         ->orWhere('email', 'LIKE', "%$searchQuery%")
         ->get();
 
         return response()->json([
         'remitentes' => $resultado,
        ]);
     }

     // TODO: merodos para obtener algun remitente

     public function obtenerRemitentesPJET()
     {
         $resultado = Remitentes_PJET::all();
         return response()->json([
             'remitentes' => $resultado,
         ]);
     } 

    public function obtenerRemitentesExternos()
    {
        $resultado = Remitentes_Externos::all();
        return response()->json([
            'remitentes' => $resultado,
        ]);
    }

    public function obtenerOtrosRemitentes()
    {
        $resultado = Otros_Remitentes::all();
        return response()->json([
            'remitentes' => $resultado,
        ]);
    }

    // TODO: metodos para eliminar algun tipo de remitente


    public function eliminarRemitente($id)

    {
        try {
            // Encuentra el remitente por su ID
            $remitente = Remitentes_PJET::findOrFail($id);

            // Elimina el remitente
            $remitente->delete();

            return response()->json(['message' => 'Remitente eliminado con éxito'], 200);
        } catch (\Exception $e) {
            // error si el remitente no se elimino
            return response()->json(['error' => 'Error al eliminar el remitente'], 500);
        }
    }

    public function eliminarRemitenteExterno($id)

    {
        try {
            // Encuentra el remitente por su ID
            $remitente = Remitentes_Externos::findOrFail($id);

            // Elimina el remitente
            $remitente->delete();

            return response()->json(['message' => 'Remitente eliminado con éxito'], 200);
        } catch (\Exception $e) {
            // error si el remitente no se elimino
            return response()->json(['error' => 'Error al eliminar el remitente'], 500);
        }
    }

    public function eliminarOtroRemitente($id)

    {
        try {
            // Encuentra el remitente por su ID
            $remitente = Otros_Remitentes::findOrFail($id);

            // Elimina el remitente
            $remitente->delete();

            return response()->json(['message' => 'Remitente eliminado con éxito'], 200);
        } catch (\Exception $e) {
            // error si el remitente no se elimino
            return response()->json(['error' => 'Error al eliminar el remitente'], 500);
        }
    }

    // TODO: metodos para obtener los detalles de un remitente

    public function obtenerDetallesRemitente($id = 0)
    {
        $remitente = Remitentes_PJET::findOrFail($id);
        return Inertia::render('formularios/agregarremitentes', [
            'id_componente' => 7,
            'id_remitente' => $id,
            'remitente' => $remitente,
        ]);
    }

    public function obtenerDetallesRemitenteExterno($id = 0)
    {
        $remitente = Remitentes_Externos::findOrFail($id);
        return Inertia::render('formularios/agregarremitentes', [
            'id_componente' => 10,
            'id_remitente' => $id,
            'remitente' => $remitente,
        ]);
    }

    public function obtenerDetallesOtroRemitente($id = 0)
    {
        $remitente = Otros_Remitentes::findOrFail($id);
        return Inertia::render('formularios/agregarremitentes', [
            'id_componente' => 11,
            'id_remitente' => $id,
            'remitente' => $remitente,
        ]);
    }

    //TODO: metos para actualizar algun remitente

    public function actualizarRemitente(Request $request, $id)
    {
        try {
            // Validación de datos 
            $request->validate([
                'datos_escrito_entrantes.nombre_remitente' => 'required|string|max:255',
                'datos_escrito_entrantes.apellidos' => 'required|string|max:255',
                'datos_escrito_entrantes.puesto' => 'required|string|max:255',
                'datos_escrito_entrantes.telefono_del_remitente' => 'required|string|max:255',
                'datos_escrito_entrantes.direccion_del_remitente' => 'required|string|max:255',
                'datos_escrito_entrantes.correo_electronico' => 'required|string|max:255',

            ]);

            // Encuentra el remitente existente por su ID
            $remitente = Remitentes_PJET::findOrFail($id);

            // Actualiza los campos con los datos del formulario
            $remitente->update($request->datos_escrito_entrantes);

            return response()->json(['message' => 'Cambios guardados correctamente en el registro existente']);
        } catch (\Exception $e) {
            // Manejo de errores
            return response()->json(['error' => 'Error al guardar cambios del remitente existente', 'message' => $e->getMessage()], 500);
        }
    }

    public function actualizarRemitenteExterno(Request $request, $id)
    {
        try {
            // Validación de datos 
            $request->validate([
                'datos_escrito_entrantes.nombre' => 'required|string|max:255',
                'datos_escrito_entrantes.apellidos_del_remitente' => 'required|string|max:255',
                'datos_escrito_entrantes.area' => 'required|string|max:255',
                'datos_escrito_entrantes.telefono' => 'required|string|max:255',
                'datos_escrito_entrantes.cargo' => 'required|string|max:255',


            ]);

            // Encuentra el remitente existente por su ID
            $remitente = Remitentes_Externos::findOrFail($id);

            // Actualiza los campos con los datos del formulario
            $remitente->update($request->datos_escrito_entrantes);

            return response()->json(['message' => 'Cambios guardados correctamente en el registro existente']);
        } catch (\Exception $e) {
            // Manejo de errores
            return response()->json(['error' => 'Error al guardar cambios del remitente existente', 'message' => $e->getMessage()], 500);
        }
    }

    public function actualizarOtroRemitente(Request $request, $id)
    {
        try {
            // Validación de datos 
            $request->validate([
                'datos_escrito_entrantes.nombre_del_remitente' => 'required|string|max:255',
                'datos_escrito_entrantes.apellidos_remitente' => 'required|string|max:255',
                'datos_escrito_entrantes.direccion' => 'required|string|max:255',
                'datos_escrito_entrantes.telefono_remitente' => 'required|string|max:255',
                'datos_escrito_entrantes.grado_de_estudios' => 'required|string|max:255',
                'datos_escrito_entrantes.email' => 'required|string|max:255',


            ]);

            // Encuentra el remitente existente por su ID
            $remitente = Otros_Remitentes::findOrFail($id);

            // Actualiza los campos con los datos del formulario
            $remitente->update($request->datos_escrito_entrantes);

            return response()->json(['message' => 'Cambios guardados correctamente en el registro existente']);
        } catch (\Exception $e) {
            // Manejo de errores
            return response()->json(['error' => 'Error al guardar cambios del remitente existente', 'message' => $e->getMessage()], 500);
        }
    }
}
