<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Acuse;
use App\Models\DocumentoContestacion;
use App\Models\Documentos;

class ArchivadosController extends Controller
{
    public function index()
    {
        
        return Inertia::render('documentos/archivados', [
            'id_componente' => 4
        ]);
    }

    public function obtenerDocumentosArchivados()
{
    try {
        // Obtener la lista de documentos con estado 4 (Archivados)
        $archivados = Documentos::where('estado_id', 4)->get();

        // Obtener información adicional de documentos archivados
        $archivadosConArchivo = $archivados->map(function ($documento) {
            $documentoContestacion = DocumentoContestacion::where('documento_id', $documento->id)->first();
            $acuse = Acuse::where('documento_id', $documento->id)->first();
            return [
                'documento' => [
                    'id' => $documento->id,
                    'nombre_entrante' => $documento->nombre_entrante,
                    'asunto' => $documento->asunto,
                    'ubicacion_del_departamento' => $documento->ubicacion_del_departamento,
                    'fechado' => $documento->fechado,
                    'recibido' => $documento->recibido,
                    'numero_de_documento' => $documento->numero_de_documento,
                    'numero_de_folio' => $documento->numero_de_folio,
                    'a_quien_va_dirigido' => $documento->a_quien_va_dirigido,
                ],
                'nombre_archivo' => $documentoContestacion ? $documentoContestacion->nombre_documento : null,
                'nombre_acuse' => $acuse ? $acuse->nombre_acuse : null,
            ];
        });

        return response()->json([
            'archivados' => $archivadosConArchivo,
        ]);
    } catch (\Exception $e) {
        return response()->json(['message' => 'Error al obtener documentos archivados', 'error' => $e->getMessage()], 500);
    }
}

    public function eliminarDocumentoArchivado($archivadoid)
    {

    try {
        // Encuentra el documento a eliminar en DocumentoContestacion
        $documentoContestacion = DocumentoContestacion::where('nombre_documento', $archivadoid)->first();

        if (!$documentoContestacion) {
            return response()->json(['message' => 'Documento no encontrado en DocumentoContestacion'], 404);
        }

        // Obtiene el nombre del documento relacionado
        $documentoNombre = $documentoContestacion->nombre_documento;

        if (!$documentoNombre) {
            return response()->json(['message' => 'Documento relacionado no encontrado en Documentos'], 404);
        }

        // Elimina el registro en DocumentoContestacion
        $documentoContestacion->delete();

         // Encuentra el documento a eliminar en Acuses
         $acuse = Acuse::where('nombre_acuse', $archivadoid)->first();

         if (!$acuse) {
             return response()->json(['message' => 'Documento no encontrado en DocumentoAcuse'], 404);
         }
 
         // Obtiene el nombre del documento relacionado
         $documentoAcuse = $acuse->nombre_acuse;
 
         if (!$documentoAcuse) {
             return response()->json(['message' => 'Documento relacionado no encontrado en Documentos'], 404);
         }
 
         // Elimina el registro en DocumentoContestacion
         $acuse->delete();

        // Elimina el documento en Documentos
        $documento = Documentos::where('nombre_documento', $documentoNombre)->first();
        
        if ($documento) {
            $documento->delete();
        }

        return response()->json(['message' => 'Documento y datos adicionales eliminados con éxito']);
        } catch (\Exception $e) {
        return response()->json(['message' => 'Error al eliminar el documento', 'error' => $e->getMessage()], 500);
    }

    }

 
}
