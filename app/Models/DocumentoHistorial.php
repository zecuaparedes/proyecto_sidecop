<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentoHistorial extends Model
{
    use HasFactory;

    protected $table = 'historial_documentos';
    public $timestamps = true;
    protected $fillable = [
        'documento_id', 
        'estado_anterior', 
        'estado_nuevo', 
        'fecha',
    ];

public function documento()
    {
        return $this->belongsTo(Documentos::class, 'documento_id');
    }
}

