<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Areas_Poder_Judicial extends Model
{
    use HasFactory;
    protected $table = 'areas_poder_judicial'; 

    protected $fillable = [
        'nombre_del_area',
        'cargo_del_area',
        'direccion',
        'nombre_del_titular',
        'primer_apellido',
        'segundo_apellido',
        'puesto',
        'telefono',
        'correo_electronico',
    ];

    public function AreaPJET()
    {
        return $this->hasOne(Documentos::class, 'areas_poder_judicial_id');
    }
    public function AreaMovimiento()
    {
        return $this->hasOne(Movimientos::class, 'areas_poder_judicial_id');
    }
}
