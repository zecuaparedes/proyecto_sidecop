<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Remitentes_PJET extends Model
{
    use HasFactory;
    protected $table = 'remitentes_pjet'; 

    protected $fillable = [
        'nombre_remitente',
        'apellidos',
        'puesto',
        'telefono_del_remitente',
        'direccion_del_remitente',
        'correo_electronico',
    ];

    public function DocumentosPjet()
    {
        return $this->hasOne(Documentos::class, 'remitentes_pjet_id');
    }

    public function MovimientosPJET()
    {
        return $this->hasOne(Movimientos::class, 'remitentes_pjet_id');

    }
};
