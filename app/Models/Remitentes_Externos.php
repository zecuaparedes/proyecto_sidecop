<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Remitentes_Externos extends Model
{
    use HasFactory;
    protected $table = 'remitentes_externos'; 

    protected $fillable = [
        'nombre',
        'apellidos_del_remitente',
        'area',
        'telefono',
        'cargo',
       
    ];

    public function RemitentesDocumentos()
    {
        return $this->hasOne(Documentos::class, 'remitentes_externos_id');
    }
    
    public function MovimientoExterno()
    {
        return $this->hasOne(Movimientos::class, 'remitentes_externos_id');
    }
}
