<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentoContestacion extends Model
{
    use HasFactory;
    protected $table = 'documentos_contestaciones';

    protected $fillable = [
        'documento_id',
        'respuesta',
        'fecha_respuesta',
        'responsable',
        'nombre_documento',
        
    ];

    // TODO: Relación con la tabla 'documentos'
    public function documento()
    {
        return $this->belongsTo(Documentos::class, 'documento_id');
    }
}
