<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Actividades extends Model
{
    use HasFactory;
    protected $table = 'actividades';

    protected $fillable = [
        'nombre_entrante',
        'asunto',
        'ubicacion_del_departamento',
        'fechado', 
        'recibido',
        'numero_de_documento',
        'numero_de_folio',
        'a_quien_va_dirigido', 
    ];

    // Relación con la tabla 'documentos'
    public function documento()
    {
        return $this->belongsTo(Documentos::class, 'documento_id');
    }

    // Relación con la tabla 'documentos_contestaciones'
    public function documentoContestacion()
    {
        return $this->belongsTo(DocumentoContestacion::class, 'documento_contestacion_id');
    }
}
