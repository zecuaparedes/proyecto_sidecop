<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movimientos extends Model
{
    use HasFactory;
    protected $table = 'movimientos'; 

    protected $fillable = [
        'documentos_id',
        'documento_nombre',
        'fecha_del_movimiento',
        'remitentes_poder_judicial_id',
        'remitentes_externos_id',
        'destinatarios_id',
        'usuario_responsable_del_movimiento',
        'tipo_movimiento_id',
        'tipo_movimiento_nombre',
        'entrada_salida',
        'asunto_movimiento',
    ];

    //TODO: funciones del modelo movimientos y de la tabla movimientos que tienen relacion con las tablas de la base de datos.


    public function MovimientosPJET()
    {
        return $this->hasMany(Movimientos::class, 'remitentes_pjet_id');
    }
    public function MovimientoExterno()
    {
        return $this->hasMany(Movimientos::class, 'remitentes_externos_id');
    }
    public function OtroMovimiento()
    {
        return $this->hasMany(Movimientos::class, 'otros_remitentes_id');
    }
    public function MovimientoArea()
    {
        return $this->hasMany(Movimientos::class, 'areas_poder_judicial_id');
    }
    public function MovimientoDestinatario()
    {
        return $this->hasMany(Movimientos::class, 'destinatarios_id');
    }
    public function CatMovimientos()
    {
        return $this->hasOne(Movimientos::class, 'tipo_movimiento_id');
    }

    public function Movimientos()
    {
        return $this->hasMany(Movimientos::class, 'documentos_id');
    }

    //TODO: Establece las relaciones con las tablas remitentes_PJET remitentes_Externos y Otros_remitentes con el modelo movimientos.

    public function documento()
    {
        return $this->belongsTo(Documentos::class, 'documento_id');
    }
    
    public function oficinaOrigen()
    {
        return $this->belongsTo(Remitentes_PJET::class, 'oficina_de_origen');
    }

    public function oficinaDestino()
    {
        return $this->belongsTo(Destinatarios::class, 'oficina_de_destino');
    }

    public function remitentePjet()
    {
        return $this->belongsTo(Remitentes_PJET::class, 'remitentes_pjet_id');
    }

    public function remitenteExterno()
    {
        return $this->belongsTo(Remitentes_Externos::class, 'remitentes_externos_id');
    }

    public function otroRemitente()
    {
        return $this->belongsTo(Otros_Remitentes::class, 'otros_remitentes_id');
    }


}
