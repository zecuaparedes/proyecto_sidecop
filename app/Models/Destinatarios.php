<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destinatarios extends Model
{
    use HasFactory;

    protected $table = 'destinatarios';

    protected $guarded = [
        'nombre',
        'apellidos',
        'puesto',
        'telefono',
        'direccion',
        'correo_electronico',
    ];

    
    public function DestinatarioDocumento()
    {
        return $this->hasOne(Documentos::class, 'destinatarios_id');
    }
    public function MovimientoDestinatario()
    {
        return $this->hasOne(Movimientos::class, 'destinatarios_id');
    }
}
