<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Acuse extends Model
{
    use HasFactory;
    protected $table = 'acuses';

    protected $fillable = [
        'acuse_id',
        'nombre_firmante',
        'fecha_firma',
        'documento_id',
        'documento_contestacion_id',
        'nombre_acuse',
        
    ];

    // Relación con la tabla 'documentos'
    public function documento()
    {
        return $this->belongsTo(Documentos::class, 'documento_id');
    }

    // Relación con la tabla 'documentos_contestaciones'
    public function documentoContestacion()
    {
        return $this->belongsTo(DocumentoContestacion::class, 'documento_contestacion_id');
    }
}
