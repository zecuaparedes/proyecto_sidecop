<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatEstado extends Model
{
    use HasFactory;
    protected $table = 'cat_estados';

    protected $fillable = [
        'nombre',
        'descripcion',
        'activo',
        'created_at',
        'updated_at',
        'deleted_at',
        
    ];

    /**
     * Relación con la tabla 'documentos'.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function documentos()
    {
        return $this->hasMany(Documentos::class, 'estado_id');
    }
}
