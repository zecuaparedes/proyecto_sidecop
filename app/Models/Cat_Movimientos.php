<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cat_Movimientos extends Model
{
    use HasFactory;

    protected $table = 'cat_movimientos';
    
    protected $fillable = [
        'tipo_movimiento',
        'tipo_activo',
    ];

    protected $dates = [
        'fecha_creacion',
        'fecha_modificacion',
    ];

    public function CatMovimientos()
    {
        return $this->hasOne(Movimientos::class, 'tipo_movimiento_id');

    }
}
