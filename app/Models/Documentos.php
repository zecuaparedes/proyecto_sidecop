<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Documentos extends Model
{
    use HasFactory;
    
    protected $table = 'documentos';
    protected $fillable = [
            'nombre_entrante',
            'asunto',
            'ubicacion_del_departamento',
            'fechado', 
            'recibido',
            'numero_de_documento',
            'numero_de_folio',
            'a_quien_va_dirigido',
            'estado_id',
            'importante',
           
               
    ];


    //TODO: funciones del modelo documentos y de la tabla documentos que tienen relacion con las tablas de la base de datos.

    public function DocumentosPjet()
    {
        return $this->hasMany(Documentos::class, 'remitentes_pjet_id');
    }

    public function historialDocumentos()
    {
        return $this->hasMany(DocumentoHistorial::class, 'documento_id');
    }

    public function RemitentesDocumentos()
    {
        return $this->hasMany(Documentos::class, 'remitentes_externos_id');
    }
    public function OtrosDocumentos()
    {
        return $this->hasMany(Documentos::class, 'otros_remitentes_id');
    }
    public function DestinatarioDocumento()
    {
        return $this->hasMany(Documentos::class, 'destinatarios_id');
    }

    public function AreaPJET()
    {
        return $this->hasMany(Documentos::class, 'areas_poder_judicial_id');
    }


    public function movimientos()
    {
        return $this->hasMany(Movimientos::class);
    }

    

    //TODO: Establece las relaciones con las tablas remitentes_PJET remitentes_Externos y Otros_remitentes con el modelo documentos.

    public function remitentePjet()
    {
        return $this->belongsTo(Remitentes_PJET::class, 'remitentes_pjet_id');
    }

    public function remitente_Externo()
    {
        return $this->belongsTo(Remitentes_Externos::class, 'remitentes_externos_id');
    }

    public function otroRemitente()
    {
        return $this->belongsTo(Otros_Remitentes::class, 'otros_remitentes_id');
    }

    public function estado()
    {
        return $this->belongsTo(CatEstado::class, 'estado_id');
    }


}
