<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Otros_Remitentes extends Model
{
    use HasFactory;
    protected $table = 'otros_remitentes'; 

    protected $fillable = [
        'nombre_del_remitente',
        'apellidos_remitente',
        'direccion',
        'telefono_remitente',
        'grado_de_estudios',
        'email',
       
    ];

    public function OtrosDocumentos()
    {
        return $this->hasOne(Documentos::class, 'otros_remitentes_id');
    }

    public function OtroMovimiento()
    {
        return $this->hasOne(Movimientos::class, 'otros_remitentes_id');
    }
}
